package escuela.java;

import java.io.IOException;

/**
 * Created by hgranjal on 18/06/2018.
 */
public class CostCalculator {
    private Shape shape;

    public CostCalculator(Shape shape) {
        this.shape = shape;
    }

    public double calculateCost() {
        return calculateFenceCost() + calculateGrassCost();
    }

    private double calculateFenceCost() {
        double fenceCost = 0d;
        try {
            fenceCost = CostConfiguration.getInstance().getFenceCost() * shape.calculatePerimeter();
        } catch (IOException e) {
            System.out.println("Erro while calculating fence cost: " + e.getMessage());
        }
        return fenceCost;
    }

    private double calculateGrassCost() {
        double grassCost = 0d;
        try {
            grassCost = CostConfiguration.getInstance().getGrassCost() * shape.calculateArea();
        } catch (IOException e) {
            System.out.println("Erro while calculating fence cost: " + e.getMessage());
        }
        return grassCost;
    }
}
