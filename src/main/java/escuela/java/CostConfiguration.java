package escuela.java;

import com.google.inject.Singleton;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by hgranjal on 18/06/2018.
 */
public class CostConfiguration {

    private static CostConfiguration instance;
    private Properties costsProps = new Properties();

    private CostConfiguration() {
    }

    public static CostConfiguration getInstance() throws IOException {
        if (instance == null) {
            instance = new CostConfiguration();
            instance.costsProps.load(new FileInputStream("src/main/Resources/CostConfiguration.properties"));
        }
        return instance;
    }


    public double getFenceCost() {
        return Double.valueOf(costsProps.getProperty("escuela.java.costs.fence"));
    }

    public double getGrassCost() {
        return Double.valueOf(costsProps.getProperty("escuela.java.costs.grass"));
    }
}
