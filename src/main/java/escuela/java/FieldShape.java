package escuela.java;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by hgranjal on 18/06/2018.
 */
public abstract class FieldShape implements Shape {

    public double calculateCost() {
        return calculateFenceCost() + calculateGrassCost();
    }

    private double calculateFenceCost() {
        double fenceCost = 0d;
        try {
            fenceCost = CostConfiguration.getInstance().getFenceCost() * calculatePerimeter();
        } catch (IOException e) {
            System.out.println("Erro while calculating fence cost: " + e.getMessage());
        }
        return fenceCost;
    }

    private double calculateGrassCost() {
        double grassCost = 0d;
        try {
            grassCost = CostConfiguration.getInstance().getGrassCost() * calculateArea();
        } catch (IOException e) {
            System.out.println("Erro while calculating fence cost: " + e.getMessage());
        }
        return grassCost;
    }
}
