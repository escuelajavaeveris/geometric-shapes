package escuela.java;

/**
 * Created by hgranjal on 18/06/2018.
 */
public interface Shape {
    double calculateArea();
    double calculatePerimeter();
}
