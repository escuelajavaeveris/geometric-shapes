package escuela.java;

/**
 * Created by hgranjal on 18/06/2018.
 */
public class Triangle extends FieldShape {
    private double a,b,c;

    public Triangle() {
    }

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calculateArea() {
        double s = calculatePerimeter()/2;
        return Math.sqrt(s * (s-a) * (s-b) * (s-c));
    }

    //Cachear???
    @Override
    public double calculatePerimeter() {
        return a + b + c;
    }

    //public abstract double findHeight();
}
