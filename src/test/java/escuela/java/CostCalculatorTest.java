package escuela.java;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;

/**
 * Created by hgranjal on 18/06/2018.
 */
public class CostCalculatorTest {

    CostCalculator costCalculator;
    @Mock
    Triangle triangle;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        costCalculator = new CostCalculator(triangle);
    }

    @Test
    public void testCalculateCost() {
        when(triangle.calculatePerimeter()).thenReturn(11d);
        when(triangle.calculateArea()).thenReturn(4.146d);
        Assert.assertEquals(costCalculator.calculateCost(), 213.65d, 0.01d);
    }


}
