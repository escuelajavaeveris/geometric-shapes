package escuela.java;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Created by hgranjal on 18/06/2018.
 */
public class TriangleTest {

    Triangle triangle = new Triangle(3,3,5);
    @Spy
    Triangle triangleSpy = spy(triangle);

//    @BeforeMethod
//    public void init() {
//        MockitoAnnotations.initMocks(this);
//    }

    @Test
    public void testCalculatePerimeter() {
        Assert.assertEquals(triangle.calculatePerimeter(), 11d);
    }

    @Test
    public void testCalculateArea() throws IOException {
        Assert.assertEquals(triangle.calculateArea(), 4.146d, 0.001d);
    }

    @Test
    public void testCalculateCost() {
        when(triangleSpy.calculatePerimeter()).thenReturn(11d);
        when(triangleSpy.calculateArea()).thenReturn(4.146d);
        Assert.assertEquals(triangleSpy.calculateCost(), 213.65d, 0.01d);
    }
}
